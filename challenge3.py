def nchoosek(n, k): # n choose k is used on the wiki article
    return (math.factorial(n)) / (math.factorial(k) * math.factorial(n-k))

def pascal(rows):
    triangle = [] # the triangle will be a list of list

    for x in range(rows): 
        nextrow = [] # holds the values for the next row to be added
        
        for y in range(x+1): # the length of each row is the row number + 1
            nextrow.append(nchoosek(x,y)) # use the n choose k function for every value

        triangle.append(nextrow)
    return triangle