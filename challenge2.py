def binarycount(i):

    counter = 0
    j = bin(i)
    for digit in str(j):
        if digit == "1":
            counter += 1

    return counter