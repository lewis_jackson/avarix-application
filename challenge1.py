matrix = [[0 for x in range(7)] for x in range(6)]
matrix[0][0] = "r"
matrix[0][1] = "r"
matrix[0][2] = "r"
matrix[0][3] = "r"
matrix[0][4] = "g"
matrix[0][5] = "g"
matrix[0][6] = "b"
matrix[1][0] = "r"
matrix[1][1] = "r"
matrix[1][2] = "r"
matrix[1][3] = "g"
matrix[1][4] = "g"
matrix[1][5] = "g"
matrix[1][6] = "b"
matrix[2][0] = "r"
matrix[2][1] = "r"
matrix[2][2] = "g"
matrix[2][3] = "g"
matrix[2][4] = "g"
matrix[2][5] = "b"
matrix[2][6] = "b"
matrix[3][0] = "b"
matrix[3][1] = "b"
matrix[3][2] = "g"
matrix[3][3] = "g"
matrix[3][4] = "y"
matrix[3][5] = "y"
matrix[3][6] = "b"
matrix[4][0] = "b"
matrix[4][1] = "g"
matrix[4][2] = "g"
matrix[4][3] = "g"
matrix[4][4] = "r"
matrix[4][5] = "r"
matrix[4][6] = "b"
matrix[5][0] = "b"
matrix[5][1] = "b"
matrix[5][2] = "b"
matrix[5][3] = "b"
matrix[5][4] = "b"
matrix[5][5] = "b"
matrix[5][6] = "b"


def countries(matrix):

    countrylist = {}

    for loop in range(10): # check 10 times
        for x in range(6): # assumed 2D array was same dimensions as example given
            for y in range(7): # two for statements exhaust each coordinate in array
                element = matrix[x][y]
                if not element in countrylist:
                    countrylist[element] = [(x,y)] # the coordinates of each part of the country are stored
                else:
                    for z in range(len(countrylist[element])):
                        if (x == countrylist[element][z][0]) or (y == countrylist[element][z][1]): # if this element is on the same row or column as duplicate character
                            countrylist[element].append((x,y))
                            next

                countryset = set() #removing duplicates from lists
                nodupes = []
                for a in countrylist[element]:
                    if a not in countryset:
                        nodupes.append(a)
                        countryset.add(a)
                countrylist[element] = nodupes
    
    for (key, value) in countrylist.items():
        print key, len(value)